%global _grubthemedir /boot/grub2/themes

Name:       unity-logos
Version:    30.0
Release:    1%{?dist}
Summary:    Icons and pictures

Group:      System Environment/Base
URL:        http://unityproject.org
Source0:    %{name}-%{version}.tar.xz
#The KDE Logo is under a LGPL license (no version statement)
License:    GPLv2 and LGPLv2+
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:  noarch

Obsoletes:  redhat-logos
Obsoletes:  generic-logos < 17.0.0-5
Provides:   redhat-logos = %{version}-%{release}
Provides:   system-logos = %{version}-%{release}

Obsoletes:  fedora-logos
Provides:   fedora-logos
Conflicts:  anaconda-images <= 10
Conflicts:  redhat-artwork <= 5.0.5
BuildRequires: hardlink
# For _kde4_* macros:
BuildRequires: kde-filesystem
# For generating the EFI icon
BuildRequires: libicns-utils
Requires:   plymouth-plugin-script
Requires(post): coreutils

%description
The unity-logos package contains various image files which can be
used by the bootloader, anaconda, and other related tools.

%package httpd
Summary: Fedora-related icons and pictures used by httpd
Provides: system-logos-httpd = %{version}-%{release}
Provides: fedora-logos-httpd = %{version}-%{release}
Obsoletes: system-logos-httpd
Obsoletes: fedora-logos-httpd
Obsoletes:  generic-logos < 17.0.0-5
BuildArch: noarch

%description httpd
The generic-logos-httpd package contains image files which can be used by
httpd.

%prep
%setup -q

%build
make

%install
rm -rf %{buildroot}

# should be ifarch i386
mkdir -p %{buildroot}/boot/grub
install -p -m 644 bootloader/splash.xpm.gz %{buildroot}/boot/grub/splash.xpm.gz
# end i386 bits


mkdir -p %{buildroot}%{_datadir}/firstboot/themes/generic
for i in firstboot/* ; do
  install -p -m 644 $i %{buildroot}%{_datadir}/firstboot/themes/generic
done

mkdir -p %{buildroot}%{_datadir}/pixmaps/bootloader
install -p -m 644 bootloader/fedora.icns %{buildroot}%{_datadir}/pixmaps/bootloader
install -p -m 644 bootloader/fedora.vol %{buildroot}%{_datadir}/pixmaps/bootloader
install -p -m 644 bootloader/fedora-media.vol  %{buildroot}%{_datadir}/pixmaps/bootloader

mkdir -p %{buildroot}%{_datadir}/anaconda/pixmaps/workstation
install -p -m 644 workstation/sidebar-bg.png %{buildroot}%{_datadir}/anaconda/pixmaps/workstation/
install -p -m 644 workstation/sidebar-logo.png %{buildroot}%{_datadir}/anaconda/pixmaps/workstation/
install -p -m 644 workstation/topbar-bg.png %{buildroot}%{_datadir}/anaconda/pixmaps/workstation/

mkdir -p %{buildroot}%{_datadir}/pixmaps/splash
for i in gnome-splash/* ; do
  install -p -m 644 $i %{buildroot}%{_datadir}/pixmaps/splash
done

mkdir -p %{buildroot}%{_datadir}/pixmaps
for i in pixmaps/* ; do
  install -p -m 644 $i %{buildroot}%{_datadir}/pixmaps
done

mkdir -p %{buildroot}%{_kde4_iconsdir}/oxygen/48x48/apps/
install -p -m 644 icons/Fedora/48x48/apps/* %{buildroot}%{_kde4_iconsdir}/oxygen/48x48/apps/
mkdir -p %{buildroot}%{_kde4_appsdir}/ksplash/Themes/Leonidas/2048x1536
install -p -m 644 ksplash/SolarComet-kde.png %{buildroot}%{_kde4_appsdir}/ksplash/Themes/Leonidas/2048x1536/logo.png

mkdir -p $RPM_BUILD_ROOT%{_datadir}/plymouth/themes/unity/
for i in plymouth/unity/* ; do
  install -p -m 644 $i $RPM_BUILD_ROOT%{_datadir}/plymouth/themes/unity/
done

mkdir -p $RPM_BUILD_ROOT%{_grubthemedir}/unity
for i in bootloader/unity/*.png ; do
  install -p -m 644 $i $RPM_BUILD_ROOT%{_grubthemedir}/unity
done
mkdir -p $RPM_BUILD_ROOT%{_grubthemedir}/unity/progress_bar
for i in bootloader/unity/progress_bar/*.png ; do
  install -p -m 644 $i $RPM_BUILD_ROOT%{_grubthemedir}/unity/progress_bar
done
install -p -m 644 bootloader/unity/dejavu_bold_14.pf2 $RPM_BUILD_ROOT%{_grubthemedir}/unity/dejavu_bold_14.pf2
install -p -m 644 bootloader/unity/dejavu_12.pf2 $RPM_BUILD_ROOT%{_grubthemedir}/unity/dejavu_12.pf2
install -p -m 644 bootloader/unity/theme.txt $RPM_BUILD_ROOT%{_grubthemedir}/unity/theme.txt

# File or directory names do not count as trademark infringement
mkdir -p %{buildroot}%{_datadir}/icons/Fedora/48x48/apps/
mkdir -p %{buildroot}%{_datadir}/icons/Fedora/scalable/apps/
install -p -m 644 icons/Fedora/48x48/apps/* %{buildroot}%{_datadir}/icons/Fedora/48x48/apps/
install -p -m 644 icons/Fedora/scalable/apps/* %{buildroot}%{_datadir}/icons/Fedora/scalable/apps/

mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps
install -p -m 644 icons/Fedora/scalable/apps/anaconda.svg $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps/anaconda.svg

mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/places
install -p -m 644 pixmaps/fedora-logo-sprite.svg $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/places/start-here.svg

(cd anaconda; make DESTDIR=%{buildroot} install)

# save some dup'd icons
/usr/sbin/hardlink -v %{buildroot}/

cd %{buildroot}%{_datadir}/anaconda/boot
ln splash.png syslinux-splash.png
cd -


%post
plymouth-set-default-theme unity -R || :
touch --no-create %{_datadir}/icons/Fedora || :
touch --no-create %{_kde4_iconsdir}/oxygen ||:

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_datadir}/icons/Fedora || :
touch --no-create %{_kde4_iconsdir}/oxygen ||:
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  if [ -f %{_datadir}/icons/Fedora/index.theme ]; then
    gtk-update-icon-cache --quiet %{_datadir}/icons/Fedora || :
  fi
  if [ -f %{_kde4_iconsdir}/Fedora-KDE/index.theme ]; then
    gtk-update-icon-cache --quiet %{_kde4_iconsdir}/Fedora-KDE/index.theme || :
  fi
fi
fi

%posttrans
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  if [ -f %{_datadir}/icons/Fedora/index.theme ]; then
    gtk-update-icon-cache --quiet %{_datadir}/icons/Fedora || :
  fi
  if [ -f %{_kde4_iconsdir}/oxygen/index.theme ]; then
    gtk-update-icon-cache --quiet %{_kde4_iconsdir}/oxygen/index.theme || :
  fi
fi


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING COPYING-kde-logo README
%{_datadir}/firstboot/themes/*
%{_datadir}/anaconda/boot/*
%{_datadir}/anaconda/pixmaps/*
%{_datadir}/icons/Fedora/*/apps/*
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/icons/hicolor/*/places/*
%{_datadir}/pixmaps/*
%exclude %{_datadir}/pixmaps/poweredby.png
%{_datadir}/plymouth/themes/unity/*
%{_kde4_appsdir}/ksplash/Themes/Leonidas/2048x1536/logo.png
%{_kde4_iconsdir}/oxygen/
%{_grubthemedir}/unity/*
# should be ifarch i386
/boot/grub/splash.xpm.gz
# end i386 bits

%files httpd
%doc COPYING
%{_datadir}/pixmaps/poweredby.png

%changelog
* Fri Jan 18 2019 JMiahMan <JMiahMan@unity-linux.org> - 29.0-4
- Add extlinux background.

* Thu Jan 17 2019 JMiahMan <JMiahMan@unity-linux.org> - 29.0-3
- Add grub2 bootsplash theme.

* Wed Jan 16 2019 JMiahMan <JMiahMan@unity-linux.org> - 29.0-2
- Update some Icons for Unity 29 release.

* Sun Aug 20 2017 Ian Firns <firnsy@unityproject.org> - 26.0-1
- Update for Korora 26 release.
